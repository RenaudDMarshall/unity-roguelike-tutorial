﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Actor : MonoBehaviour {

    public MapHandler MapHandler;
    public InputHandler InputHandler;
    public char character;
    public Color color;
    public float movementTime = 0.1f;

    private IEnumerator moveCoroutine;
    private bool moving;

	// Use this for initialization
	void Start ()
    {
        gameObject.GetComponent<SpriteRenderer>().color = color;
        gameObject.GetComponentInChildren<TextMesh>().text = character.ToString();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Application.isPlaying == false)
        {
            // Editor only code - Calls if the object data is changed
            gameObject.GetComponent<SpriteRenderer>().color = color;
            gameObject.GetComponentInChildren<TextMesh>().text = character.ToString();
        }
        else
        {
            // Gameplay code
        }
    }

    public void Move(int deltaX, int deltaY)
    {
        if (moving == false)
        {
            if (moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
            }

            moveCoroutine = MoveCoroutine(deltaX, deltaY);
            StartCoroutine(moveCoroutine);
        }
    }

    private IEnumerator MoveCoroutine(int deltaX, int deltaY)
    {
        moving = true;
        Vector3 startPosition = transform.position;
        Vector3 endPosition = new Vector3((int)transform.position.x + deltaX, (int)transform.position.y + deltaY, transform.position.z);


        GameObject target = MapHandler.Access((int)endPosition.x, -(int)endPosition.y);
        if (target == null || target.GetComponent<BoxCollider2D>() != null)
        {
            moving = false;
            yield break;
        }

        for (float t = 0; t <= 1 * movementTime; t += Time.deltaTime)
        {
            transform.position = Vector3.Lerp(startPosition, endPosition, t / movementTime);
            yield return 0;
        }

        transform.position = endPosition;
        moving = false;
    }

}
