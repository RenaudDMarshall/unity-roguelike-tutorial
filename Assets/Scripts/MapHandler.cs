﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapHandler : MonoBehaviour {

    public GameObject tilePrefab;
    public GameObject wallPrefab;
    public int maxColumns;
    public int maxRows;
    public GameObject[,] map;


    void Start()
    {
        map = new GameObject[maxColumns, maxRows];
        for (int i = 0; i < maxColumns; i++)
        {
            for (int j = 0; j < maxRows; j++)
            {
                if (Random.Range(0, 5) == 4)
                {
                    GameObject background = (GameObject)Instantiate(wallPrefab, Vector3.zero, Quaternion.identity);
                    background.transform.position = new Vector3(i, -j, 0);
                    background.transform.parent = transform;
                    map[i, j] = background;
                }
                else
                {
                    GameObject background = (GameObject)Instantiate(tilePrefab, Vector3.zero, Quaternion.identity);
                    background.transform.position = new Vector3(i, -j, 0);
                    background.transform.parent = transform;
                    map[i, j] = background;
                }
            }
        }
    }

    public GameObject Access(int x, int y)
    {
        if (x < 0 || x >= maxColumns || y < 0 || y >= maxRows)
        {
            return null;
        }
        else
        {
            return map[x, y];
        }
    }
}
