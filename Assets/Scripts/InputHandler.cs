﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {

    public Actor Actor;

	void Update ()
    {
        if (UnityEngine.Input.GetKeyDown(KeyCode.A) == true)
        {
            Actor.Move(-1, 0);
        }
        else if (UnityEngine.Input.GetKeyDown(KeyCode.D) == true)
        {
            Actor.Move(1, 0);
        }
        else if (UnityEngine.Input.GetKeyDown(KeyCode.W) == true)
        {
            Actor.Move(0, 1);
        }
        else if (UnityEngine.Input.GetKeyDown(KeyCode.S) == true)
        {
            Actor.Move(0, -1);
        }
        else if (UnityEngine.Input.GetKeyDown(KeyCode.Escape) == true)
        {
            Application.Quit();
            Debug.Log("quit");
        }
        else if (UnityEngine.Input.GetKeyDown(KeyCode.BackQuote) == true)
        {
            Screen.fullScreen = !Screen.fullScreen;
            Debug.Log("fullscreen toggle");
        }
    }
}
